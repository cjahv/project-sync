#!/usr/bin/env bash
action=

cols=`tput cols`
row=$(( (cols - 51) / 2 ))

k_s=$(printf "%-${row}s" "=")
k_l=$((cols/21))

k_header_s=$(printf "%-${k_l}s" "=")
k_header_s=${k_header_s// /=}
k_header_s=${k_header_s//=/%-21s }

menus=()

for name in `ls ${SHELL_COMMAND}`;do
    menus=(${menus[@]} ${name})
done

commandText=()

while [ "$action" != "q" ];do
    echo "========================${k_s// /=}==========================${k_s// /=}="
    echo "=  ____    ___    __ __=${k_s// /=}=《配置文件向导程序》=====${k_s// /=}="
    echo "= / __ \  / _ |  / //_/=${k_s// /=}=在下面选择你要执行的操作=${k_s// /=}="
    echo "=/ /_/ / / __ | / ,<   =${k_s// /=}=输入对应的数字序号=======${k_s// /=}="
    echo "=\____/ /_/ |_|/_/|_|  =${k_s// /=}=按照提示操作即可=========${k_s// /=}="
    echo "=                      =${k_s// /=}==========================${k_s// /=}="
    echo "========================${k_s// /=}==========================${k_s// /=}="

    for ((i=0;i<${#menus[*]};i++));do
        commandText=(${commandText[@]} "$(($i+1)).${menus[$i]%.*}")
        if [ "$k_l" == "${#commandText[*]}" ];then
            printf "${k_header_s}\n" ${commandText[@]}
            commandText=()
        fi
    done
    commandText=(${commandText[@]} "q.退出")
    printf "${k_header_s}\n" ${commandText[@]}
    commandText=()

    printf "请选择："
    read action
    if [ "$action" -gt 0 ] 2>/dev/null ;then
        command=${menus[$(($action-1))]}
        if [ "$command" ];then
            . ${SHELL_COMMAND}/${command}
            if [ "$?" == "3" ];then
                exit 0
            fi
        fi
    elif [ "$action" != "q" ];then
        echo "无效的输入"
        sleep 1
    fi
done