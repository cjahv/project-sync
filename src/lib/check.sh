#!/usr/bin/env bash

if [ ! -d ".git" ]; then
    echo "请在配置项目中运行"
    exit 1
fi

home=`echo ~`

if [ `pwd` == "$home" ];then
    echo "不能再用户根目录执行"
    exit 1
fi

if [[ ! -f "$home/.ssh/id_rsa" || ! -f "$home/.ssh/id_rsa.pub" ]];then
    echo "检测到你还没有设置默认秘钥，现在你需要添加一对秘钥..."
    ssh-keygen -N "" -t rsa -f ~/.ssh/id_rsa -q
    echo "秘钥已添加！"
fi

. ${SHELL_LIB}/check-server.sh