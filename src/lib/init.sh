#!/usr/bin/env bash
SHELL_COMMAND=$SHELL_FOLDER/../src/command
SHELL_LIB=$SHELL_FOLDER/../src/lib

. ${SHELL_LIB}/function.sh

. ${SHELL_LIB}/check.sh

. ${SHELL_LIB}/menus.sh