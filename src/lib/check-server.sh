#!/usr/bin/env bash
if [[ ! -f '.remember_ip' || ! -f '.remember_user' ]];then
    printf "首次运行，请输入要连接的服务器："
    read REPLY
    echo $REPLY > .remember_ip
    echo "服务器ip已保存在项目根目录下的\".remember_ip\"，之后可以修改此文件来改变连接服务器"
    printf "请输入连接${REPLY}的用户名："
    read REPLY
    echo ${REPLY:-root} > .remember_user
    echo "用户名已保存在项目根目录下的\".remember_user\""
fi