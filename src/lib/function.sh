#!/usr/bin/env bash
checkIP(){
    IP=$1
    if [ `echo $IP | awk -F . '{print NF}'` -ne 4 ];then
        echo "Wrong IP!"
    else
        a=`echo $IP | awk -F . '{print $1}'`
        b=`echo $IP | awk -F . '{print $2}'`
        c=`echo $IP | awk -F . '{print $3}'`
        d=`echo $IP | awk -F . '{print $4}'`
        if [[ $a -gt 0 && $a -le 255 ]] && [[ $b -ge 0 && $b -le 255 ]] && [[ $c -ge 0 && $c -le 255 ]] && [[ $d -gt 0 && $d -lt 255 ]];then
            echo "Right IP!"
        else
            echo "Wrong IP!"
        fi
    fi
}