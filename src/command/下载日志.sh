#!/usr/bin/env bash
. ${SHELL_LIB}/check.sh

user=`cat .remember_user`
server=`cat .remember_ip`

services=`cat docker-compose.yml|grep "^  \w"|sed "s/[ :\r]//g"`
services=`echo $services`

if [ ! -d "logs" ]; then
    echo "创建日志文件夹logs"
    mkdir logs
fi

for service in ${services};do
    echo "下载$service的日志..."
    ssh ${user}@${server} ". /etc/profile;cd ~/${PWD##*/};docker-compose logs $service" > logs/${service}.log
done