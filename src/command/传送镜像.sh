#!/usr/bin/env bash
. ${SHELL_LIB}/check.sh

images=`cat docker-compose.yml|grep image:|awk '{print $2}'|sort -n|awk '{if($0!=line)print; line=$0}'|sed "s/\r//"`
images=`echo $images`

user=`cat .remember_user`
server=`cat .remember_ip`

if [ "images" ];then
    echo "正在导出镜像 ${images}"
    docker save ${images} | gzip -c > tmp_images.tar
    if [ "$?" == "0" ];then
        echo "正在上传镜像..."
        scp tmp_images.tar $user@$server:~
        if [ "$?" == "0" ];then
            ssh $user@$server ". /etc/profile;gunzip -c tmp_images.tar|docker load&&rm tmp_images.tar"
        else
            echo "镜像上传失败！"
        fi
    else
        echo "镜像导出失败！"
    fi
    rm tmp_images.tar
else
    echo "没有任何镜像"
fi
