#!/usr/bin/env bash
. ${SHELL_LIB}/check.sh
git reset --hard HEAD
git pull

user=`cat .remember_user`
server=`cat .remember_ip`

ssh $user@$server "mkdir -p ~/${PWD##*/}"
if [ -f ".syncfile" ];then
    for i in `cat .syncfile`;do
        i=`echo $i|sed "s/\s$//g"`
        scp -r ${i} $user@$server:~/${PWD##*/}
    done
else
    scp -r . $user@$server:~/${PWD##*/}
fi