#!/usr/bin/env bash
. ${SHELL_LIB}/check.sh

user=`cat .remember_user`
server=`cat .remember_ip`

ssh-copy-id -i ~/.ssh/id_rsa.pub $user@$server