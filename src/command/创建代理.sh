#!/usr/bin/env bash
. ${SHELL_LIB}/check.sh

user=`cat .remember_user`
server=`cat .remember_ip`

printf "请输入要开启的端口(默认7070)："
read REPLY

ssh -qTCnfND 0.0.0.0:${REPLY:-7070} $user@$server