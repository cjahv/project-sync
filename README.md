# 橡树配置项目管理工具

## 使用

使用本工具需要在linux下运行，或先安装git，然后通过下面命令安装本工具

```bash
# 选其中一个

# 源码中国服务器
sh -c "$(curl -fsSL https://gitee.com/jahv/project-sync/raw/master/install.sh)"
# gitlab服务器
sh -c "$(curl -fsSL https://gitlab.com/cjahv/project-sync/raw/master/install.sh)"
# 公司服务器（需要vpn）
sh -c "$(curl -fsSL https://git.oakhit.com/docker-config/project-sync/raw/master/install.sh)"
```