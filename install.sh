#!/usr/bin/env bash
home=`echo ~`
now=`pwd`

if [ ! -d "${home}/.project-sync" ]; then
    cd ~
    git clone --depth 1 https://git.oakhit.com/docker-config/project-sync.git ~/.project-sync
    cd ${now}
else
    git -C ~/.project-sync reset --hard master
    git -C ~/.project-sync pull
fi

if [ ! -x "${home}/.project-sync/bin/oak" ]; then
    chmod +x ${home}/.project-sync/bin/oak
fi

if [ -d "/usr/local" ];then
    if [ ! -f "/usr/local/bin/oak" ];then
        ln -s $home/.project-sync/bin/oak /usr/local/bin/oak
    fi
else
    if [ ! -f "/usr/bin/oak" ];then
        ln -s $home/.project-sync/bin/oak /usr/bin/oak
        if [ "$?" != "0" ];then
            if [ ! -f "$home/.bash_profile" ];then
                echo "PATH=${home}/.project-sync/bin:\$PATH" > $home/.bash_profile
            else
                if [ ! `cat $home/.bash_profile|grep .project-sync` ];then
                    echo "PATH=${home}/.project-sync/bin:\$PATH" >> $home/.bash_profile
                fi
            fi
            . $home/.bash_profile
        fi
    fi
fi

oak